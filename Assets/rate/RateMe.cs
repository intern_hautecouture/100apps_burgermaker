﻿using UnityEngine;
using System.Collections;

public class RateMe : MonoBehaviour {

	public SpriteRenderer BTSprite;
	public Sprite[] ButtonSpries;

	public SpriteRenderer[] StarSprites;
	public Sprite[] StarButtonSpries;

	public GameObject RateButton;
	private string email="ur.emailID";
	public string subject="Pool Mania Feedback";
	private string body="Please share your valuable feedback and suggestions so that we can improve the game.";
	public string BundleId;

	// Use this for initialization
	void Start () {
		BTSprite.sprite = null;
		RateButton.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private int Starval;

	 void OnStar(GameObject button)
	{


	}
	//;

	public void OnStarHover(tk2dUIItem button)
	{
		RateButton.SetActive(true);

		Starval = int.Parse(button.name);
		
		if(Starval <= 2)
		{
			BTSprite.sprite = ButtonSpries[0];

		}else{
			BTSprite.sprite = ButtonSpries[1];

		}


		for(int i = 0; i < Starval; i++)
		{
			StarSprites[i].sprite = StarButtonSpries[1];

		}
		
		for(int j = Starval; j < StarSprites.Length;j++)
		{

			StarSprites[j].sprite = StarButtonSpries[0];

		}
		

	}


	public void OnRate()
	{
		if(Starval <= 2)
		{
			
			subject = WWW.EscapeURL(subject);
			body = "Please share your valuable feedback and suggestions so that we can improve the game.";
			body = WWW.EscapeURL(body);
			Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
			
			////			EtceteraBinding.showMailComposer( "", "", "Please share your valuable feedback and suggestions so that we can improve the game.", true );
			//#else			// i.e the android devices
			//
			////			EtceteraAndroid.showEmailComposer( "", "", "Please share your valuable feedback and suggestions so that we can improve the game.", true );
			//			Application.OpenURL("mailto:" + email + "?subject:" + subject + "&body:" + body);
			//
			//#endif
			
		}else{
			#if UNITY_ANDROID
			{
				Application.OpenURL("https://play.google.com/store/apps/details?id=com.bundle.ID");
			}
			#elif UNITY_IPHONE
			Application.OpenURL("https://itunes.apple.com/us/forIphoneThings");
			
			#endif
		}
		
	}
	
	public void OnDismiss()
	{
		//this.gameObject.SetActive(false);
		GameObject.FindObjectOfType<ReviewPanelScript> ().DeactivatateReviewButtonsFromDialogueBox ();
	}
	
}