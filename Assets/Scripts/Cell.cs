﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour
{

    private string currentColor = "";
    private int rowIndex, colIndex;

    public string CurrentColor
    {
        get
        {
            return currentColor;
        }
        set
        {
            currentColor = value;
        }
    }

    public int RowIndex
    {
        get
        {
            return rowIndex;
        }
        set
        {
            rowIndex = value;
        }
    }

    public int ColIndex
    {
        get
        {
            return colIndex;
        }
        set
        {
            colIndex = value;
        }
    }
}