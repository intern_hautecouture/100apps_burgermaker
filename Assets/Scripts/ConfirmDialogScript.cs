﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConfirmDialogScript : MonoBehaviour {

//	public GameObject soundToggle;
	
	void OnEnable()
	{
		GameManager.Instance.IsConfirmDialogShowing = true;

	}
	
//	void OnDisable()
//	{
//		GameManager.Instance.IsConfirmDialogShowing = false;
//		
//		if (GameManager.Instance.IsSound)
//		{
//			soundToggle.GetComponent<tk2dUIToggleButton>().IsOn = true;
//			Camera.main.GetComponent<AudioSource>().audio.Play();
//		}
//		else
//		{
//			soundToggle.GetComponent<tk2dUIToggleButton>().IsOn = false;
//			Camera.main.GetComponent<AudioSource>().audio.Stop();
//		}
//	}
//	
	
	public void OnOkButtonClick()
	{
		StartCoroutine(OnOkButtonAction());
	}
	
	private IEnumerator OnOkButtonAction()
	{
		//AdmobManager.DestroyAds ();
		yield return new WaitForSeconds(0.5f);
		Application.Quit();
	}
	
	public void OnCancelButtonClick()
	{
		StartCoroutine(OnCancelButtonAction());
	}
	
	private IEnumerator OnCancelButtonAction()
	{
		yield return new WaitForSeconds(0.5f);
		gameObject.SetActive(false);
	}

}
