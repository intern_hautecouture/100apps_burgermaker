﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasCtrl : MonoBehaviour {

	//[SerializeField] Text levelText;
    [SerializeField] tk2dTextMesh level;
    //[SerializeField] Vector2 posDiff_hide;
    [SerializeField] Vector2 posDiff_hide2;
    //Vector2 levelTextFirstPos;
    Vector2 levelFirstPos;
	void Awake()
	{
		//levelTextFirstPos = levelText.rectTransform.anchoredPosition;
        levelFirstPos = level.transform.position;

    }

	// Use this for initialization
	void Start () {

		HideLevelText ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetLevelTextPos()
	{
		//levelText.rectTransform.anchoredPosition = levelTextFirstPos;
        level.transform.position = levelFirstPos;
	}
	public void HideLevelText()
	{
		//levelText.rectTransform.anchoredPosition = levelTextFirstPos + posDiff_hide;
        level.transform.position = levelFirstPos + posDiff_hide2;
	}

	public void InputLevelText(string str)
	{
		//levelText.text = str;
        level.text = str;
	}
}
