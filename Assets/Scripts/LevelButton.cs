﻿using UnityEngine;
using System.Collections;

public class LevelButton : MonoBehaviour {

    public static bool tutorial = false;
    private int levelNumber;

    public int LevelNumber
    {
        get
        {
            return levelNumber;
        }
        set
        {
            levelNumber = value;
        }
    }

    public GameObject buttonGuardObject;
    private GameObject buttonGuardInstance;

    void OnLevelButtonClick()
    {
		MainMenuScript.instance.OnPlayButtonClickSOUND ();

		if(PlayerPrefs.GetInt("Level") == 0){
            tutorial=true;
            StartCoroutine(InstructionButtonAction());
            
		}else{
			StartCoroutine(LevelButtonAction());
		}
    }

	private IEnumerator InstructionButtonAction()
	{
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        GameManager.Instance.LevelId = levelNumber;	//レベルは設定しておく
		Camera.main.SendMessage("activeLevelSelectObject", false, SendMessageOptions.RequireReceiver);
		Camera.main.SendMessage("absolutelyActiveInstructionObject", true, SendMessageOptions.RequireReceiver);
	}

    IEnumerator LevelButtonAction()
    {
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        GameManager.Instance.LevelId = levelNumber;
        //GameManager.Instance.GameTime = 31f;
        Camera.main.SendMessage("activeLevelSelectObject", false, SendMessageOptions.RequireReceiver);
        var levelObject = Instantiate(GameManager.Instance.levelObject, new Vector3(0f, 0f, 0f), Quaternion.identity);
        levelObject.GetComponent<GameController>().Timer = 31f;
    }

}
