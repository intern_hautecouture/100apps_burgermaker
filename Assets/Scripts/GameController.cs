﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NendUnityPlugin.AD;
using NendUnityPlugin.Common;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour
{
	public static GameController instance;
    private string currentColor = "";
    private bool isDrawing = true, isComplete = false, isLineComplete = false;
    private int cellTypeCounter = 0, flowCounter = 0, totalFlow;
    private GameObject touchedObject;

    public AudioClip matchLineClip, lineCutClip;

    public tk2dTextMesh flowText;
    public tk2dTextMesh levelText;
	public tk2dTextMesh timeText;
	public bool checkForLineToExceed = false;

	public GameObject goTimeText;

	bool checkForPreviousLinesWithSameColor = false;
	bool checkForOutOfBoundary = false;
    float time;
	bool checkForReverseOfDirection = false;
	int reverseOfDirection = 0;
	bool checkForAdsOnce = false;

	int countForNumberOfTilesChangedAfterAttachment = 1, checkForWhiteClones = 1;
	bool checkForNumberOfTilesChangedAfterAttachment = false;

	//Text levelText;
	//CanvasCtrl canvasCtrl;

	string InitialSelectedGameObject;

	private Vector2 PreVector;
	private Vector2 CurrentVector;

    private GameObject LevelSelect;

	private bool stageClear = false;	//追加(2015/8/26) 原田貴広

	int showAddCounter = 0;

	string spotId;

	//Transform trfmTimeText;

	Vector3 orgTimeTextScale;

	bool clockSE = false;
	AudioSource clockAS;

	IEnumerator instructionCoroutine;

	GameObject[,] cellObjects;
	GameObject gameControllerInstance;

	const string jpCode = "jp";
	const string zhCNCode = "zh-CN";
	const string thCode = "th";

    void Start()
    {

        int a;
        a = PlayerPrefs.GetInt("Level") + 1;

        //Debug.Log("ddd" + 1);
        //time = 31f;
		Invoke ("loadAD", 2.0f);
		instance = this;
        //int[] levelCompeleteData = LevelData.levelCompleteData[GameManager.Instance.LevelId - 1];
        //totalFlow = levelCompeleteData[levelCompeleteData.Length - 1] == 0 ? 4 : 5;
        //totalFlow = levelCompeleteData.Length;
        flowText.text = "Flow : 0 / " + totalFlow;

		string levelStr = "Level: ";

/*		canvasCtrl = GameObject.Find("Canvas").GetComponent<CanvasCtrl>();

		canvasCtrl.SetLevelTextPos ();
		canvasCtrl.InputLevelText(levelStr + GameManager.Instance.LevelId);

        GameManager.Instance.CurrentScene = Constants.GAME_SCENE;
*/
        levelText.text = levelStr + GameManager.Instance.LevelId;
        
        /*
		#if UNITY_IPHONE
		NendAdInterstitial.Instance.Load(
			"d46d71585a9ace1a3c77019a78139d409db704f9", 
			"435263");
		spotId = "435263";
		#elif UNITY_ANDROID
		NendAdInterstitial.Instance.Load(
			"351c9ce80a5ce823e5b5d5931c1f22672710a950", 
			"435259");
		spotId = "435259";
		#endif
		*/

        //GameObject.Find ("NendAdBanner").
        //	GetComponent<NendAdBanner>().Show();

        //trfmTimeText = transform.FindChild ("TimeText");

        orgTimeTextScale = timeText.scale;

		clockAS = gameObject.GetComponent<AudioSource>();

		instructionCoroutine = ShowInstruction ();
		if(PlayerPrefs.GetInt("Level") == 0)
		{
			StartCoroutine(instructionCoroutine);
		}

		Invoke ("goTimeTestActive", .0f);

		SetCellObjects ();
    }

    public float Timer
    {
        get
        {
            return time;
        }
        set
        {
            time = value;
        }
    }

	void SetCellObjects(){
		int cellWidth = 
			LevelData.levelData[GameManager.Instance.LevelId - 1].GetLength(0);

		cellObjects = new GameObject[cellWidth, cellWidth];

		gameControllerInstance = 
			GameObject.FindGameObjectWithTag(Constants.TAG_GAME_CONTROLLER);
		GameObject[] cellType = 
			gameControllerInstance.GetComponent<ObjectCollection>().cellType;

		for(int i=0; i<cellType.Length; ++i){
            Debug.Log("celltype: " + cellType[i].tag);

            GameObject[] cells;

            try
            {
                cells = GameObject.FindGameObjectsWithTag(cellType[i].tag);
            }
            catch (Exception e)
            {
                Debug.Log("celltype.tag exception");
                continue;
            }
			

			foreach(GameObject go in cells){
				if(go.transform.parent == transform){
					int rowIndex = go.GetComponent<Cell>().RowIndex;
					int columnIndex = go.GetComponent<Cell>().ColIndex;

					cellObjects[rowIndex, columnIndex] = go;
				}
			}
		}


		for(int i=0; i<cellWidth; ++i){
			for(int j=0; j<cellWidth; ++j){
				Debug.Log("row: " + i + ", col: " + j + 
				          "; " + cellObjects[i,j].name);
			}
		}

	}

	void goTimeTestActive(){
		goTimeText.SetActive (true);
	}

	IEnumerator ShowInstruction()
	{
		float waitTime = .5f;

		MainMenuScript.instance.showingInstruction = true;

		while(true){
			yield return new WaitForSeconds (waitTime);
			
			MainMenuScript.instance.instructionPanel.SetActive (
				!MainMenuScript.instance.instructionPanel.activeSelf && 
				MainMenuScript.instance.showingInstruction);
		}

		//MainMenuScript.instance.showingInstruction = false;
	}

    public void loadAD()
    {
        Debug.Log("Load started in Game Controller");
        //AdmobAd.Instance ().LoadInterstitialAd ();

        NendAdInterstitial.Instance.Load();
	}


	void ShowAD(){
		if (showAddCounter == 0) {
            float showAdProbability = UnityEngine.Random.Range(0.0f, 1.0f);
            Debug.Log("prob: " + showAdProbability);

            if (showAdProbability <= 0.3f) {
                Debug.Log("Show Ad in Game Controller");
                NendAdInterstitial.Instance.Show();
            }

			//admob interstitial
			//AdMobBannerInterstitial.Instance.ShowBanner ();
			//AdmobManager.ShowInterstitial ();
		}
		showAddCounter++;
	}


	public void toggleDrawingMode()
	{
		Debug.Log ("yoyo");
		if (isDrawing)
			isDrawing = false;
		else
			isDrawing = true;

		Invoke ("toggleDrawingMode2", 2.0f);
	}
	void toggleDrawingMode2()
	{
		if (isDrawing)
			isDrawing = false;
		else
			isDrawing = true;
		
		
	}


    void Update()
    {


		timeText.text = "" + (int)time;

		if(!stageClear)	//追加(2015/8/26) 原田貴広 if文だけ
		time -= Time.deltaTime;

		if((int)time < 10){
			float remainder = Mathf.Sin((-(time - (int)time) + 1) * Mathf.PI);
			//trfmTimeText.localScale = 
			//	new Vector3(1 + remainder, 1 + remainder, 1);
			timeText.scale = orgTimeTextScale + 
				new Vector3(remainder * remainder, remainder * remainder, 0);

			float colorValue = Mathf.Max(time / 10.0f, 0);

			timeText.color = new Color(1, colorValue, colorValue);

			if(!clockSE){
				clockSE = true;
				clockAS.Play();

			}
		}

        if (Input.GetButtonDown("Fire1") && isDrawing)
        {
            //			Debug.Log("BDown");
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out hit)) return;
            if (hit.collider == null)return;

            checkForPreviousLinesWithSameColor = true;
            string tagName = hit.collider.tag;

            //最初にタッチした球orパネル
            GameObject cellObject = hit.collider.gameObject;


            currentColor = "";


            //Whiteだったらreturn。
            if (!GameManager.Instance.CellColorTagList.Contains(tagName)) return;
               
                currentColor = tagName;
                isLineComplete = true;



                if (tagName == "Green")
                {
                    GameObject[] a = GameObject.FindGameObjectsWithTag("White");
                    foreach (GameObject yydh in a)
                    {
                        if (yydh.GetComponent<tk2dSprite>().spriteId == 5 ||
                            yydh.GetComponent<tk2dSprite>().spriteId == 7)
                        {
                            //									Debug.Log("already thr");
                            yydh.GetComponent<tk2dSprite>().spriteId = 13;
                            //									isLineComplete = false;
                            //									isComplete = false;
                            ChangeCustomColor();
                        }
                    }
                }
                if (tagName == "Red")
                {
                    GameObject[] a = GameObject.FindGameObjectsWithTag("White");
                    foreach (GameObject yydh in a)
                    {
                        if (yydh.GetComponent<tk2dSprite>().spriteId == 4 ||
                            yydh.GetComponent<tk2dSprite>().spriteId == 10)
                        {
                            //									Debug.Log("already thr");
                            yydh.GetComponent<tk2dSprite>().spriteId = 13;
                            Debug.Log("yydh:");
                            //									isLineComplete = false;
                            //									isComplete = false;
                            ChangeCustomColor();

                        }
                    }
                }
                if (tagName == "Blue")
                {
                    GameObject[] a = GameObject.FindGameObjectsWithTag("White");
                    foreach (GameObject yydh in a)
                    {
                        if (yydh.GetComponent<tk2dSprite>().spriteId == 0 ||
                            yydh.GetComponent<tk2dSprite>().spriteId == 9)
                        {
                            //									Debug.Log("already thr");
                            yydh.GetComponent<tk2dSprite>().spriteId = 13;
                            //									isLineComplete = false;
                            //									isComplete = false;
                            ChangeCustomColor();

                        }
                    }
                }
                if (tagName == "Yellow")
                {
                    GameObject[] a = GameObject.FindGameObjectsWithTag("White");
                    foreach (GameObject yydh in a)
                    {
                        if (yydh.GetComponent<tk2dSprite>().spriteId == 1 ||
                            yydh.GetComponent<tk2dSprite>().spriteId == 11)
                        {
                            //									Debug.Log("already thr");
                            yydh.GetComponent<tk2dSprite>().spriteId = 13;
                            //									isLineComplete = false;
                            //									isComplete = false;
                            ChangeCustomColor();

                        }
                    }
                }
                if (tagName == "Pink")
                {
                    GameObject[] a = GameObject.FindGameObjectsWithTag("White");
                    foreach (GameObject yydh in a)
                    {
                        if (yydh.GetComponent<tk2dSprite>().spriteId == 3 ||
                            yydh.GetComponent<tk2dSprite>().spriteId == 12)
                        {
                            //									Debug.Log("already thr");
                            yydh.GetComponent<tk2dSprite>().spriteId = 13;
                            //									isLineComplete = false;
                            //									isComplete = false;
                            ChangeCustomColor();

                        }
                    }
                }




                //現在タッチしている色の球
                touchedObject = hit.collider.gameObject;

                AddIntoColorCellList(cellObject);
    
        }

        else if (Input.GetButton("Fire1") && isDrawing)
        {
            //			Debug.Log("BFire");
            RaycastHit hit;
            //			checkForNumberOfTilesChangedAfterAttachment = true;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out hit)) return;
            if (hit.collider == null) return;

            string tagName = hit.collider.tag;

            GameObject cellObject = hit.collider.gameObject;

            if (touchedObject == null) return;


            if (cellObject.name != touchedObject.name && cellObject.name == "BlankCell(Clone)")
            {
                isDrawing = false;
            };

            //★
            if (cellObject.name != touchedObject.name && cellObject.name != "WhiteCell(Clone)")
            {
                Debug.Log("Not Not");
                isDrawing = false;
                ChangeCustomColor();
            }

            if (tagName.Equals(Constants.TAG_WHITE))
            {
                ChangeObjectColor(cellObject);
            }
            else
            {

                if (touchedObject != null && !touchedObject.Equals(cellObject) && 
				    checkCell(tagName))
          
                {
                    ChangeObjectColor(cellObject);


                    if (tagName.Equals(currentColor))
                        DecideLineComplete(true, cellObject);
                    else
                        isDrawing = false;
                    //							Debug.Log("Kaun hai");
                }
            }
                
                
            
            
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            			Debug.Log("upper");
            WhenTouchNoMore();
        }
        WonDecision();

    }

	bool checkCell(string tag){
		bool ret = false;

		switch(tag){
		case Constants.TAG_BLANK:
		case Constants.TAG_BLUE:
		case Constants.TAG_GREEN:
		case Constants.TAG_PINK:
		case Constants.TAG_RED:
		case Constants.TAG_WHITE:
		case Constants.TAG_YELLOW:
			ret = true;
			break;
		}

		return ret;
	}

    void WhenTouchNoMore()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider == null) return;

            string tagName = hit.collider.tag;

            if (tagName.Equals(Constants.TAG_WHITE))
            {
                if (!checkForPreviousLinesWithSameColor) return;
                ChangeCustomColor();										//////////////////////////////////////////////////
                checkForPreviousLinesWithSameColor = false;
            }



            else
            {
                GameObject cellObject = hit.collider.gameObject;
                if (touchedObject != null && 
                    isDrawing &&
                    !touchedObject.Equals(cellObject))
                {
                    if (tagName.Equals(currentColor))
                    {
                        AddIntoColorCellList(cellObject);
                        Debug.Log("mouch!!");
                    }
                   
                }
                else
                {
                    SetDetaultRemainingList(currentColor);
                    if (touchedObject != null &&
                        touchedObject.Equals(cellObject))
                        DecreaseFlowCounter();
                }
            }

               
        }
        else
        {
            SetDetaultRemainingList(currentColor);
        }

        currentColor = "";
        isDrawing = true;
        WinDecision();
       
		GameObject[] cellType = gameControllerInstance
			.GetComponent<ObjectCollection> ().cellType;
		for(int i=0; i<Constants.FOOD_COUNT; ++i)
		{
			string foodTag = cellType[i].tag;

			//食べ物がボード上にあるか
			if(CellExists(foodTag)){
				//食べ物がつながっているか
				if(!FoodConnected(foodTag)){
					DeleteLine(foodTag);
				}
			}
		}

    }


    private void AddIntoColorCellList(GameObject cellObject)
    {
        int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);
        if (indexOfCurrentColor != -1)
        {
 

            List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCurrentColor];
            if (!cellList.Contains(cellObject))
                cellList.Add(cellObject);

            //Debug.Log("list size : " + cellList.Count);
        }

    }

    private void ChangeObjectColor(GameObject cellObject)
    {

        int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);
        if (indexOfCurrentColor != -1 && 
            !cellObject.CompareTag("Destroyer"))
        {

            if (cellObject.GetComponent<Cell>().CurrentColor != "" && 
                cellObject.GetComponent<Cell>().CurrentColor != currentColor)		
			{
                AdjustCellProperty(cellObject);
//				WhenTouchNoMore();
//				ChangeCustomColor();
//				isDrawing= false;
//				Debug.Log("yo");
			}

            List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCurrentColor];

            if (!IsDrawingDiagonal(cellObject, cellList) &&
                !cellList.Contains(cellObject) && 
                cellList.Count < 2 && cellObject.CompareTag(Constants.TAG_WHITE))
            {
                cellList.Add(cellObject);
                ChangeCellProperty(indexOfCurrentColor, cellObject);
                ChangeSpriteAsDirection(cellObject);
            }
            else
            {
                if (cellList.Count > 1)
                {
                    int indexOfCurrentCell = cellList.IndexOf(cellObject);
                    if (!IsDrawingDiagonal(cellObject, cellList) && !cellList.Contains(cellObject))
                    {
                        cellList.Add(cellObject);
                        if (cellObject.CompareTag(Constants.TAG_WHITE))
                            ChangeCellProperty(indexOfCurrentColor, cellObject);

                        ChangeSpriteAsDirection(cellObject);
                    }
                }
            }
        }
    }


    //★
    private void AdjustCellProperty(GameObject cellObject)
    {
//        Debug.Log("Adjust cell property");
        string cellColor = cellObject.GetComponent<Cell>().CurrentColor;
        int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);
        GameManager.Instance.ColorCompletionList[indexOfCurrentColor] = false;
        List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCurrentColor];
        cellList.Remove(cellObject);

        //Debug.Log("Cell Color : " + cellColor + " Current Color : " + currentColor);
        if (cellColor != currentColor)
        {
            SetDetaultRemainingList(cellColor);
            DecreaseFlowCounter();
        }
    }

    private void SetDetaultRemainingList(string cellColor)
    {

        int indexOfCellColor = GameManager.Instance.CellColorTagList.IndexOf(cellColor);

        if (indexOfCellColor != -1)
        {
            List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCellColor];

            for (int i = 0; i < cellList.Count; i++)
            {
                if (cellList[i].CompareTag(Constants.TAG_WHITE) && !cellList[i].CompareTag(cellColor))
                {
                    cellList[i].transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
                    cellList[i].GetComponent<tk2dSprite>().SetSprite(Constants.SPRITE_BOX_WHITE);
                    cellList[i].GetComponent<Cell>().CurrentColor = "";

					//15/8/27 原田
					int rowIndex = cellList[i].GetComponent<Cell>().RowIndex;
					int colIndex = cellList[i].GetComponent<Cell>().ColIndex;
					cellList[i].transform.position = 
						CellPosition.cell_xy[rowIndex, colIndex] + 
							new Vector3(0, LevelGenerator.translate, 0) + 
							new Vector3(2.5f * LevelGenerator.translate_h, 0, 0);
                }
            }
            cellList.Clear();

            //GameManager.Instance.ColorCompletionList[indexOfCellColor] = false;
            if (GameManager.Instance.IsSound)
				AudioSource.PlayClipAtPoint(lineCutClip, new Vector3(), 
				                            MainMenuScript.instance.volumeSE);
        }


    }

    private void DecreaseFlowCounter()
    {
        flowCounter--;
        flowText.text = "Flow : " + flowCounter + " / " + totalFlow;
        int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);
        
		try{
			GameManager.Instance.ColorCompletionList[indexOfCurrentColor] = false;
		}catch{
			Debug.LogError("indexOfCurrentColor: " + indexOfCurrentColor);
			Debug.LogError("ColorCompletionList.Count: " + 
			          GameManager.Instance.ColorCompletionList.Count);
		}
		//GameManager.Instance.ColorCompletionList[indexOfCurrentColor] = false;
    }

    private bool IsDrawingDiagonal(GameObject cellObject, List<GameObject> cellList)
    {
        Vector2 curVec = new Vector2(cellObject.GetComponent<Cell>().RowIndex, cellObject.GetComponent<Cell>().ColIndex);

		Vector2 prevVec = Vector2.zero;

		checkForReverseOfDirection = true;

		if(cellList.Count>0)
         prevVec = new Vector2(cellList[cellList.Count - 1].GetComponent<Cell>().RowIndex, cellList[cellList.Count - 1].GetComponent<Cell>().ColIndex);



        if (curVec.x == prevVec.x || curVec.y == prevVec.y)
            return false;

        return true;
    }


    private void ChangeCellProperty(int indexOfCurrentColor, GameObject cellObject)
    {
        cellObject.GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.StraightSpriteNameList[indexOfCurrentColor]);
        cellObject.GetComponent<Cell>().CurrentColor = currentColor;
    }


	private float getScale()
	{
		int[,] levelData = LevelData.levelData[GameManager.Instance.LevelId - 1];
		float scale = 1f;
		switch (levelData.Length)
		{
		case 25://5マス
			scale = 1f;
			break;
		case 36://6マス
			scale = 0.8f;
			break;
		}

		return scale;
	}

	//0以上360未満の角度を返す
	int GetNormalizedAngle(float angle){
		int ret = Mathf.RoundToInt(angle);

		while(ret < 0){
			ret += 360;
		}

		int quotient = ret / 360;
		ret = ret - quotient*360;

		return ret;
	}

	//指定した食べ物の線を消す
	void DeleteLine(string foodTag)
	{
		foreach(GameObject cell in cellObjects){
			if(cell.tag == "White"){
				tk2dSprite sprite = cell.GetComponent<tk2dSprite>();

				if(sprite.spriteId == 
				   sprite.GetSpriteIdByName(foodTag.ToLower() + "_straight") || 
				   sprite.spriteId == 
				   sprite.GetSpriteIdByName(foodTag.ToLower() + "_turn"))
				{
					sprite.spriteId = 
						sprite.GetSpriteIdByName(Constants.SPRITE_BOX_WHITE);
				}
			}
		}
	}

	//指定した食べ物のセルがあるか
	bool CellExists(string foodTag)
	{
		bool ret = false;

		foreach(GameObject cell in cellObjects){
			if(cell.tag == foodTag){
				ret = true;
				break;
			}
		}

		return ret;
	}

	//指定した食べ物に線がつながっているか
	bool FoodConnected(string foodTag)
	{
		bool ret = false;

		List<GameObject> foodCellList = new List<GameObject> ();

		foreach(GameObject goCell in cellObjects)
		{
			if(goCell.tag == foodTag){
				foodCellList.Add(goCell);
			}
		}

		if(foodCellList.Count == 2)
		{
			bool[] connectedToNext = new bool[foodCellList.Count];

			for(int i=0; i<foodCellList.Count; i++){
				int connectedCount = 0;

				//４方向
				for(int j=0; j<4; j++){
					if(ConnectedToNextCell(foodCellList[i], j)){
						connectedCount++;
					}
				}

				if(connectedCount == 1){
					connectedToNext[i] = true;
				}
			}

			bool tempRet = true;

			foreach(bool connected in connectedToNext){
				tempRet = tempRet && connected;
			}

			ret = tempRet;
		}

		return ret;
	}

	//線がつながっているか
	bool ConnectedToNextCell(GameObject foodCellObject, int dir){
		bool ret = false;

		Cell foodCell = foodCellObject.GetComponent<Cell> ();

		int cellWidth = 
			LevelData.levelData[GameManager.Instance.LevelId - 1]
			.GetLength(0);

		int nextCellRowIndex;
		int nextCellColIndex;
		bool condition;
		int angle_straight;
		int angle_turn1, angle_turn2;

		switch(dir){
		case 0:	//right
			nextCellRowIndex = foodCell.RowIndex;
			nextCellColIndex = foodCell.ColIndex + 1;
			condition = nextCellColIndex < cellWidth;
			angle_straight = 0;
			angle_turn1 = 90;
			angle_turn2 = 180;
			break;	
		case 1:	//upper
			nextCellRowIndex = foodCell.RowIndex + 1;
			nextCellColIndex = foodCell.ColIndex;
			condition = nextCellRowIndex < cellWidth;
			angle_straight = 90;
			angle_turn1 = 180;
			angle_turn2 = 270;
			break;	
		case 2:	//left
			nextCellRowIndex = foodCell.RowIndex;
			nextCellColIndex = foodCell.ColIndex - 1;
			condition = 0 <= nextCellColIndex;
			angle_straight = 0;
			angle_turn1 = 270;
			angle_turn2 = 0;
			break;	
		case 3:	//lower
			nextCellRowIndex = foodCell.RowIndex - 1;
			nextCellColIndex = foodCell.ColIndex;
			condition = 0 <= nextCellRowIndex;
			angle_straight = 90;
			angle_turn1 = 0;
			angle_turn2 = 90;
			break;
		default:
			return ret;
		}

		if(condition){
			GameObject nextCellObject = 
				cellObjects[nextCellRowIndex, nextCellColIndex];

			tk2dSprite nextSprite = nextCellObject.GetComponent<tk2dSprite>();

			if(nextSprite == null){
				return ret;
			}

			if(nextSprite.spriteId == 
			   nextSprite.GetSpriteIdByName(
				foodCellObject.tag.ToLower() + "_straight"))
			{
				if(GetNormalizedAngle(
					nextCellObject.transform.eulerAngles.z) == angle_straight)
				{
					ret = true;
				}
			}
			else if(nextSprite.spriteId == 
			        nextSprite.GetSpriteIdByName(
				foodCellObject.tag.ToLower() + "_turn"))
			{
				if(GetNormalizedAngle(
					nextCellObject.transform.eulerAngles.z) == angle_turn1 || 
				   GetNormalizedAngle(
					nextCellObject.transform.eulerAngles.z) == angle_turn2)
				{
					ret = true;
				}
			}
		}

		return ret;
	}

    private void ChangeSpriteAsDirection(GameObject cellObject)
    {
        Vector2 currVec = new Vector2(cellObject.GetComponent<Cell>().RowIndex, cellObject.GetComponent<Cell>().ColIndex);
        int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);
        List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCurrentColor];
        int indexOfCurrentCell = cellList.IndexOf(cellObject);
        Vector2 prevVec = new Vector2(cellList[indexOfCurrentCell - 1].GetComponent<Cell>().RowIndex, cellList[indexOfCurrentCell - 1].GetComponent<Cell>().ColIndex);

        if (Mathf.Abs(prevVec.y - currVec.y) == 0)
            cellObject.transform.rotation = Quaternion.AngleAxis(90f, Vector3.forward);



		PreVector = prevVec;
		CurrentVector = currVec;
       
		Debug.Log (cellList.Count);
		foreach(GameObject go in cellList){
			Debug.Log (go.name);
		}

        
        
        if (cellList.Count > 2 && cellList[indexOfCurrentCell - 1].CompareTag(Constants.TAG_WHITE) &&
            checkForReverseOfDirection == true)
        {


            Vector2 prevToPrevVec = new Vector2(cellList[indexOfCurrentCell - 2].GetComponent<Cell>().RowIndex, 
                cellList[indexOfCurrentCell - 2].GetComponent<Cell>().ColIndex);
//			Debug.Log("prevToPrev: prevVec: currVec: "+prevToPrevVec+" : "+prevVec+" : "+currVec);

		

			if(indexOfCurrentColor == 0)
			{
				InitialSelectedGameObject = "RedCell(Clone)";
			}
			else if(indexOfCurrentColor == 1)
			{
				InitialSelectedGameObject = "GreenCell(Clone)";

			}
			else if(indexOfCurrentColor == 2)
			{
				InitialSelectedGameObject = "BlueCell(Clone)";
				
			}
			else if(indexOfCurrentColor == 3)
			{
				InitialSelectedGameObject = "YellowCell(Clone)";
				
			}
			else if(indexOfCurrentColor == 4)
			{
				InitialSelectedGameObject = "PinkCell(Clone)";
				
			} 




//			Debug.Log(indexOfCurrentColor);
		//Debug.Log("IPA"+InitialSelectedGameObject);



			if(cellObject.name == "WhiteCell(Clone)" || cellObject.name == InitialSelectedGameObject)			//either (white cell) or (another cell in end(basically same name))
			{
				checkForWhiteClones++;

//				Debug.Log("whiteCells : "+checkForWhiteClones);
			}


			if(prevToPrevVec.x >= Mathf.Abs(prevVec.x) || prevToPrevVec.y >= Mathf.Abs(prevVec.y))			// here we are checking if all cells are turned their colors or not. aint it cool?
				//maybe check if current place is white cell and at same time it isnt the starting coloured ball
			{
				countForNumberOfTilesChangedAfterAttachment++;
//				Debug.Log(countForNumberOfTilesChangedAfterAttachment);
				
			}
			
			
			if((checkForWhiteClones) != countForNumberOfTilesChangedAfterAttachment)
			{
				ChangeCustomColor();
//				Debug.Log("notConnected.yippeee");
			}




			if(Mathf.Abs(prevVec.x-currVec.x)>1)
			{
				isDrawing = false;
				isLineComplete = false;
//				Debug.Log("NotChanged");
				ChangeCustomColor();
				
				isDrawing = false;

			}

			//if(cellList.Count > 0)
			{
				/*

				//15/8/27 原田
				int colIndex = cellList[indexOfCurrentCell - 1].GetComponent<Cell>().ColIndex;
				int rowIndex = cellList[indexOfCurrentCell - 1].GetComponent<Cell>().RowIndex;

				
				List<Vector3[]> fixPattern = new List<Vector3[]>{
					new Vector3[]{//1,6
						7*Vector3.down + 4*Vector3.left,
						Vector3.zero,
						8*Vector3.up + 8*Vector3.left,
						10*Vector3.down + 4*Vector3.left,
						3*Vector3.down + 2*Vector3.left
					}, 
					new Vector3[]{//2,5
						4*Vector3.up + 5*Vector3.left,
						Vector3.zero,
						6*Vector3.up + 1*Vector3.right,
						7*Vector3.left,
						5*Vector3.left
					},
					new Vector3[]{//3,8
						6*Vector3.right + 4*Vector3.down,
						Vector3.zero,
						8*Vector3.left,
						10*Vector3.right + 5*Vector3.down,
						4*Vector3.right
					},
					new Vector3[]{//4,7
						4*Vector3.right + 5*Vector3.up,
						Vector3.zero,
						3*Vector3.right,
						4*Vector3.right + 5*Vector3.up,
						5*Vector3.up
					}
				};
				
				int[,] levelData = LevelData.levelData[GameManager.Instance.LevelId - 1];
				float scale = 1f;
				switch (levelData.Length)
				{
				case 25://5マス
					scale = 1f;
					break;
				case 36://6マス
					scale = 0.833f;
					break;
				}
				for(int i=0; i<fixPattern.Count; i++){
					for(int j=0; j<fixPattern[i].Length; j++){
						fixPattern[i][j] += 
							new Vector3(1.5f * LevelGenerator.translate_h, 0, 0);
						fixPattern[i][j] *= scale;
					}
				}

				bool change = false;

				*/

				if ((prevVec.y - prevToPrevVec.y) > 0)
				{
					// left to right movement
					if ((currVec.x - prevVec.x) < 0)
					{
						//					Debug.LogError("Here");
						//					Debug.Log(prevToPrevVec);
						
						// top to bottom movement
						if(cellList.Count > 0){
							cellList[indexOfCurrentCell - 1].transform.rotation = 
								Quaternion.AngleAxis(180f, Vector3.forward);
							cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>()
								.SetSprite(GameManager.Instance
								           .TurnSpriteNameList[indexOfCurrentColor]);
						}
						Debug.Log("1; indexOfCurrentColor = " + indexOfCurrentColor);

						/*if(change){
							Vector3[] difference = fixPattern[0];
							
							cellList[indexOfCurrentCell - 1].transform.localPosition = 
								CellPosition.cell_xy[rowIndex, colIndex] + 
									new Vector3(0, LevelGenerator.translate, 0) + 
									difference[indexOfCurrentColor];
						}
						*/
						
					}
					else if ((currVec.x - prevVec.x) > 0)
					{
						//					Debug.LogError("Here");
						//					Debug.Log(prevToPrevVec);
						
						// bottom to top movement
						if(cellList.Count > 0){
							cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(90f, Vector3.forward);
							cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
						}

						Debug.Log("2; indexOfCurrentColor = " + indexOfCurrentColor);

						/*if(change){
							Vector3[] difference = fixPattern[1];
							
							cellList[indexOfCurrentCell - 1].transform.localPosition = 
								CellPosition.cell_xy[rowIndex, colIndex] + 
									new Vector3(0, LevelGenerator.translate, 0) + 
									difference[indexOfCurrentColor];
						}
						*/
					}
					else if(Mathf.Abs(prevVec.y-currVec.y)>1 || Mathf.Abs(prevVec.x-currVec.x)>1)
					{
						//					Debug.Log("NotChanged");
						ChangeCustomColor();
						
						isDrawing = false;
						//					isDrawing = true;
						
						checkForReverseOfDirection = false;
					}
					
				}
				else if ((prevVec.y - prevToPrevVec.y) < 0)
				{
					
					// 左右変更
					if ((currVec.x - prevVec.x) < 0)
					{
						//					Debug.LogError("Here");
						//					Debug.Log(prevToPrevVec);
						
						// top to bottom movement
						if(cellList.Count > 0){
							cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(270f, Vector3.forward);
							cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
						}

						Debug.Log("3; indexOfCurrentColor = " + indexOfCurrentColor);

						/*if(change){
							Vector3[] difference = fixPattern[2];
							
							cellList[indexOfCurrentCell - 1].transform.localPosition = 
								CellPosition.cell_xy[rowIndex, colIndex] + 
									new Vector3(0, LevelGenerator.translate, 0) + 
									difference[indexOfCurrentColor];

						}
						*/

						
					}
					else if ((currVec.x - prevVec.x) > 0)
					{
						//					Debug.LogError("Here");
						//					Debug.Log(prevToPrevVec);
						
						// bottom to top movement
						if(cellList.Count > 0){
							cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
							cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
						}

						Debug.Log("4; indexOfCurrentColor = " + indexOfCurrentColor);

						/*if(change){
							Vector3[] difference = fixPattern[3];
							
							cellList[indexOfCurrentCell - 1].transform.localPosition = 
								CellPosition.cell_xy[rowIndex, colIndex] + 
									new Vector3(0, LevelGenerator.translate, 0) + 
									difference[indexOfCurrentColor];
						}
						*/

						
					}
					else if(Mathf.Abs(prevVec.y-currVec.y)>1 || Mathf.Abs(prevVec.x-currVec.x)>1)
					{
						//					Debug.Log("NotChanged");
						ChangeCustomColor();
						
						isDrawing = false;
						
						checkForReverseOfDirection = false;
						
					}
				}
				else
				{
					//				Debug.Log("prevToPrev: prevVec: currVec: "+prevToPrevVec+" : "+prevVec+" : "+currVec);
					
					if(Mathf.Abs(prevVec.x-currVec.x)>1 || Mathf.Abs(prevVec.y-currVec.y)>1)
					{
						//					Debug.Log("NotChanged");
						ChangeCustomColor();
						
						isDrawing = false;
						checkForReverseOfDirection = false;
						
					}
					
					
					else  if ((currVec.y - prevVec.y) < 0)
					{
						// right to left movement
						if ((currVec.x - prevToPrevVec.x) < 0)
						{
							//						Debug.LogError("Here");
							//Debug.Log(prevToPrevVec);
							
							// top to bottom
							if(cellList.Count > 0){
								cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(90f, Vector3.forward);
								cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
							}

							Debug.Log("5; indexOfCurrentColor = " + indexOfCurrentColor);

							/*if(change){
								Vector3[] difference = fixPattern[1];
								
								cellList[indexOfCurrentCell - 1].transform.localPosition = 
									CellPosition.cell_xy[rowIndex, colIndex] + 
										new Vector3(0, LevelGenerator.translate, 0) + 
										difference[indexOfCurrentColor];
							}
							*/

							
						}
						else if ((currVec.x - prevToPrevVec.x) > 0)
						{
							//						Debug.LogError("Here");
							//Debug.Log(prevToPrevVec);
							
							// bottom to top
							if(cellList.Count > 0){
								cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(180f, Vector3.forward);
								cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
							}

							Debug.Log("6; indexOfCurrentColor = " + indexOfCurrentColor);

							/*if(change){
								Vector3[] difference = fixPattern[0];
								
								cellList[indexOfCurrentCell - 1].transform.localPosition = 
									CellPosition.cell_xy[rowIndex, colIndex] + 
										new Vector3(0, LevelGenerator.translate, 0) + 
										difference[indexOfCurrentColor];
							}
							*/

							
						}
						//					if(Mathf.Abs(prevVec.x-currVec.x))
						//					{
						//						Debug.Log("NotChanged");
						//						ChangeCustomColor();
						//					}
						//					else
						//						Debug.Log("NotChanged");
					}
					else if ((currVec.y - prevVec.y) > 0)
					{
						// 左右スプライト変更
						if ((currVec.x - prevToPrevVec.x) < 0)
						{
							//						Debug.LogError("Here");
							//Debug.Log(prevToPrevVec);

							if(cellList.Count > 0){
								cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
								cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
							}

							Debug.Log("7; indexOfCurrentColor = " + indexOfCurrentColor);

							/*if(change){
								Vector3[] difference = fixPattern[3];
								
								cellList[indexOfCurrentCell - 1].transform.localPosition = 
									CellPosition.cell_xy[rowIndex, colIndex] + 
										new Vector3(0, LevelGenerator.translate, 0) + 
										difference[indexOfCurrentColor];
							}
							*/

							
						}
						else if ((currVec.x - prevToPrevVec.x) > 0)
						{
							//						Debug.LogError("Here");
							//Debug.Log(prevToPrevVec);

							if(cellList.Count > 0){
								cellList[indexOfCurrentCell - 1].transform.rotation = Quaternion.AngleAxis(270f, Vector3.forward);
								cellList[indexOfCurrentCell - 1].GetComponent<tk2dSprite>().SetSprite(GameManager.Instance.TurnSpriteNameList[indexOfCurrentColor]);
							}

							Debug.Log("8; indexOfCurrentColor = " + indexOfCurrentColor);

							/*if(change){
								Vector3[] difference = fixPattern[2];
								
								cellList[indexOfCurrentCell - 1].transform.localPosition = 
									CellPosition.cell_xy[rowIndex, colIndex] + 
										new Vector3(0, LevelGenerator.translate, 0) + 
										difference[indexOfCurrentColor];
							}
							*/

							
						}
						//					if(Mathf.Abs(prevVec.x-currVec.x)>1)
						//					{
						//						Debug.Log("NotChanged");
						//						ChangeCustomColor();
						//					}
						//					else
						//						Debug.Log("NotChanged");
					}
					else
					{
						if(Mathf.Abs(prevVec.x-currVec.x)>1 || Mathf.Abs(prevVec.y-currVec.y)>1)
						{
							Debug.Log("NotChanged");
							ChangeCustomColor();
							
							isDrawing = false;
							
							checkForReverseOfDirection = false;
							
						}
					}
					
				}
			}


        }

        if (!cellObject.CompareTag(Constants.TAG_WHITE))
            cellObject.transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
        
    }

    private void DecideLineComplete(bool isComplete, GameObject cellObject)
    {

		Vector2 colVec = new Vector2 (cellObject.GetComponent<Cell>().RowIndex,cellObject.GetComponent<Cell>().ColIndex);
//		Debug.Log ("Vec: "+CurrentVector+" : "+colVec+" : "+cellObject+Mathf.Abs(CurrentVector.x-colVec.x)+" : "+Mathf.Abs (CurrentVector.y-colVec.y));

		isLineComplete = false;
		if((Mathf.Abs(CurrentVector.x-colVec.x)<=1 && Mathf.Abs (CurrentVector.y-colVec.y)==0))
		{
			isLineComplete = true;
		}
		if ((Mathf.Abs (CurrentVector.x - colVec.x) == 0 && Mathf.Abs (CurrentVector.y - colVec.y) <= 1))
		{
			isLineComplete = true;
		}

		if (isLineComplete)	// there needs to be another check if all previous blocks are filled or not
        {
			if(FoodConnected(cellObject.tag))
			{
				Debug.Log("Commplete");
				isLineComplete = false;
				int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);
				//Debug.Log("Is Line Complete : " + GameManager.Instance.ColorCompletionList[indexOfCurrentColor]);
				if (!GameManager.Instance.ColorCompletionList[indexOfCurrentColor])
				{
					IncreaseFlowCounter();
				}
				WhenTouchNoMore();
				
				//			Debug.Log("yoShouldntBeComplete If NotAllTilesChange");
				
				//            Debug.Log("index of current color : " + indexOfCurrentColor + " isComplete : " + isComplete);		//////////////////////////////////////////////////////
				
				
				GameManager.Instance.ColorCompletionList[indexOfCurrentColor] = isComplete;
				if (GameManager.Instance.IsSound){
					AudioSource.PlayClipAtPoint(matchLineClip, new Vector3(), 
					                            MainMenuScript.instance.volumeSE);
					
				}
			}else{

			}
            
        }
		else
			ChangeCustomColor();

		//食べ物の絵が回ってしまっていたのを修正
		cellObject.transform.rotation = Quaternion.identity;

		/*foreach(GameObject goCell in cellObjects){
			if(goCell.tag == cellObject.tag){
				goCell.transform.rotation = Vector3.zero;
			}
		}*/

    }

    private void IncreaseFlowCounter()
    {
        flowCounter++;
        flowText.text = "Flow : " + flowCounter + " / " + totalFlow;
    }


    
    private void ChangeCustomColor()
    {

        Debug.Log("ChangeCustomColor");

		countForNumberOfTilesChangedAfterAttachment = 1;
		checkForWhiteClones = 1;

		//Debug.Log ("changeCustomColor");
		int indexOfCurrentColor = GameManager.Instance.CellColorTagList.IndexOf(currentColor);

        if (indexOfCurrentColor != -1)
        {
            List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCurrentColor];

            //Debug.Log("Change Custom Color");
            for (int i = 0; i < cellList.Count; i++)
            {
                //Whiteかつ現在とは違うタグ
                if (cellList[i].CompareTag(Constants.TAG_WHITE) && !cellList[i].CompareTag(currentColor))
                {
                    cellList[i].transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
                    cellList[i].GetComponent<tk2dSprite>().SetSprite(Constants.SPRITE_BOX_WHITE);
                    cellList[i].GetComponent<Cell>().CurrentColor = "";

					//15/8/27 原田
					int rowIndex = cellList[i].GetComponent<Cell>().RowIndex;
					int colIndex = cellList[i].GetComponent<Cell>().ColIndex;
					cellList[i].transform.position = 
						CellPosition.cell_xy[rowIndex, colIndex] + 
						new Vector3(0, LevelGenerator.translate, 0) + 
							new Vector3(2.5f * LevelGenerator.translate_h, 0, 0)
							;
                }
            }

            cellList.Clear();
        }
    }

    public void RestartCellColors()
    {
        foreach (var cellColorTag in GameManager.Instance.CellColorTagList)
        {
            int indexOfColor = GameManager.Instance.CellColorTagList.IndexOf(cellColorTag);

            if (indexOfColor != -1)
            {
                List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfColor];

                //Debug.Log("Change Custom Color");
                for (int i = 0; i < cellList.Count; i++)
                {
                    //Whiteかつ現在とは違うタグ
                    if (cellList[i].CompareTag(Constants.TAG_WHITE))
                    {
                        cellList[i].transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
                        cellList[i].GetComponent<tk2dSprite>().SetSprite(Constants.SPRITE_BOX_WHITE);
                        cellList[i].GetComponent<Cell>().CurrentColor = "";

                        //15/8/27 原田
                        int rowIndex = cellList[i].GetComponent<Cell>().RowIndex;
                        int colIndex = cellList[i].GetComponent<Cell>().ColIndex;
                        cellList[i].transform.position =
                            CellPosition.cell_xy[rowIndex, colIndex] +
                            new Vector3(0, LevelGenerator.translate, 0) +
                                new Vector3(2.5f * LevelGenerator.translate_h, 0, 0)
                                ;
                    }
                }

                cellList.Clear();

            }
        }
    }

	bool showingGameOver = false;

    private void WonDecision()
    {
		if((int)time <= 0 && !showingLevelComplete)
        {
			showingGameOver = true;

            //GameOver
            StartCoroutine(ShowGameOver());
            //Debug.Log("sssaaaa");
         
			//timeText.SortingOrder = 0;
			//clockAS.Stop();

			Invoke ("clockStop", .5f);

			StopCoroutine(instructionCoroutine);
			MainMenuScript.instance.showingInstruction = false;
			MainMenuScript.instance.instructionPanel.SetActive(false);
        }

    }

	void clockStop(){
		clockAS.Stop();
	}

    IEnumerator ShowGameOver()
    {
        checkForAdsOnce = true;

        yield return new WaitForSeconds(0.5f);

        Camera.main.SendMessage("activeGameOverObject", true, SendMessageOptions.RequireReceiver);

		/*//canvasCtrl.HideLevelText();*/
    }

	bool showingLevelComplete = false;
    //★
    private void WinDecision()
    {

        //int[] levelCompeleteData = LevelData.levelCompleteData[GameManager.Instance.LevelId - 1];
        int levelComplete = LevelData.levelComplete[GameManager.Instance.LevelId - 1];
        
        LevelSelectScript levelSelect = new LevelSelectScript();
        /*GameObject cubeobj = new GameObject("cube");
        LevelSelectScript levelSelect = cubeobj.AddComponent<LevelSelectScript>();
        Destroy(cubeobj);*/
        //int redTotal = levelCompeleteData[0];
        //int greenTotal = levelCompeleteData[1];
        //int blueTotal = levelCompeleteData[2];
        //int yellowTotal = levelCompeleteData[3];
        //int pinkTotal = levelCompeleteData[4];

        int redCellTotal = GameManager.Instance.RedCellList.Count > 0 ? GameManager.Instance.RedCellList.Count - 2 : 0;
        Debug.Log("ssssssssssssss" + GameManager.Instance.RedCellList.Count);

        int greenCellTotal = GameManager.Instance.GreenCellList.Count > 0 ? GameManager.Instance.GreenCellList.Count - 2 : 0;
        int blueCellTotal = GameManager.Instance.BlueCellList.Count > 0 ? GameManager.Instance.BlueCellList.Count - 2 : 0;
        int yellowCellTotal = GameManager.Instance.YellowCellList.Count > 0 ? GameManager.Instance.YellowCellList.Count - 2 : 0;
        int pinkCellTotal = GameManager.Instance.PinkCellList.Count > 0 ? GameManager.Instance.PinkCellList.Count - 2 : 0;


        //★★★
        //if (redCellTotal == redTotal && greenCellTotal == greenTotal && blueCellTotal == blueTotal
        //    && yellowCellTotal == yellowTotal && pinkCellTotal == pinkTotal)
        //{
        //    StartCoroutine(ShowLevelComplete());
        //    Debug.Log("WIN");
        //}

        if (redCellTotal + greenCellTotal + blueCellTotal + yellowCellTotal + pinkCellTotal == levelComplete )
        {
			if (showingGameOver){
				return;
			}else{
				showingLevelComplete = true;
			}

            StartCoroutine(ShowLevelComplete());

            int preLevel = PlayerPrefs.GetInt("Level") + 1;
			int curLevel = GameManager.Instance.LevelId;// - 1;		-1をコメントアウト(2015/8/26 原田貴広)


            if (preLevel < curLevel + 1)
            {

                PlayerPrefs.SetInt("Level", curLevel);
				PlayerPrefs.Save();		//追加(2015/8/26 原田貴広)
            }

            Debug.Log("WIN");

			clockAS.Stop();

			StopCoroutine(instructionCoroutine);
			MainMenuScript.instance.showingInstruction = false;
			MainMenuScript.instance.instructionPanel.SetActive(false);
        }



    }

    IEnumerator ShowLevelComplete()
    {
		checkForAdsOnce = true;


        yield return new WaitForSeconds(0.5f);
        Camera.main.SendMessage("activeLevelCompleteObject", true, SendMessageOptions.RequireReceiver);

		//canvasCtrl.HideLevelText();

		Invoke ("checkForAdsIncreaseValue", 1.0f);

		//追加(2015/8/26 原田貴広) =========================
		/*if(!GameManager.Instance.stageClear)
		{
			int preLevel = PlayerPrefs.GetInt("Level") + 1;
			int curLevel = GameManager.Instance.LevelId;// - 1;

			if (preLevel < curLevel)
			{
				
				PlayerPrefs.SetInt("Level", curLevel);
				PlayerPrefs.Save();
			}
		}

		GameManager.Instance.stageClear = true;
		*/
		//================================================ここまで

		stageClear = true;	//追加(2015/8/26) 原田貴広
        float timeShowAd = UnityEngine.Random.Range(0.5f, 0.7f);
        Debug.Log("timeShowAd: " + timeShowAd);
        Invoke ("ShowAD", timeShowAd);
		//ShowAD();

		//GameObject.Find ("NendAdBanner").
		//	GetComponent<NendAdBanner>().Hide();
	}
	
	void checkForAdsIncreaseValue()
	{
		if(checkForAdsOnce == true && GameObject.Find("LevelCompleteObject") != null)
		{
			Invoke("loadAD", 2.0f);
			checkForAdsOnce = false;


		}
	}

//    private bool IsLineComplete(int indexOfCurrentColor)
//    {

////		Debug.Log ("rewind");

//        int[] levelCompeleteData = LevelData.levelCompleteData[GameManager.Instance.LevelId - 1];
//        int totalCells = levelCompeleteData[indexOfCurrentColor];
//        List<GameObject> cellList = GameManager.Instance.CellColorsListCollection[indexOfCurrentColor];
//        if (totalCells == (cellList.Count - 2))
//            return true;

//        return true;

//    }


}
