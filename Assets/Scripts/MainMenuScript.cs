﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {

	public static MainMenuScript instance;
    private const string USERDATA_SOUND = "SOUND";
    public GameObject soundToggle;
    public AudioClip buttonClickSound;

	public Text instructionText;

	public GameObject instructionPanel;
	public bool showingInstruction;

	public float volumeSE = 1.0f;
    public GameObject buttonGuardObject;
    private GameObject buttonGuardInstance;
    void Start()
    {
        
		instance = this;
        if (!PlayerPrefs.HasKey(USERDATA_SOUND))
        {
            PlayerPrefs.SetInt(USERDATA_SOUND, 1);
            PlayerPrefs.Save();
            GameManager.Instance.IsSound = true;
        }


        GameManager.Instance.IsSound = PlayerPrefs.GetInt(USERDATA_SOUND) == 1 ? true : false;

        if (GameManager.Instance.IsSound)
        {
            soundToggle.GetComponent<tk2dUIToggleButton>().IsOn = true;
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().Play();
        }
        else
        {
            soundToggle.GetComponent<tk2dUIToggleButton>().IsOn = false;
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().Stop();
        }

		GameManager.Instance.CurrentScene = Constants.MAINMENU_SCENE;

		//追加(2015/8/26 原田貴広)
		transform.Find ("SoundToggleButton").gameObject.SetActive(false);
        
		instructionText.gameObject.SetActive (false);

		instructionPanel.SetActive (false);
		ScaleInstruction ();

		showingInstruction = false;
    }

	void ScaleInstruction()
	{
		int width  = Screen.width;
		int height = Screen.height;

		Debug.Log ("width: " + width);
		Debug.Log ("height: " + height);

		int orgWidth = 1080;
		int orgHeight = 1920;

		RectTransform trfm = instructionPanel.GetComponent<RectTransform>();

		float x_scale = (float)width / (float)orgWidth;
		float y_scale = (float)height / (float)orgHeight;

		Vector3 tmpScale = trfm.localScale;
		tmpScale.y *= (y_scale / x_scale);
		trfm.localScale = tmpScale;
	}

	void OnEnable()
	{
        

		if (GameManager.Instance != null)
			GameManager.Instance.CurrentScene = Constants.MAINMENU_SCENE;
        

	}

    void OnPlayButtonClick()
    {
        if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
        StartCoroutine(PlayButtonAction());
    }
	public void OnPlayButtonClickSOUND()
	{
		if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
	}


    private IEnumerator PlayButtonAction()
    {

        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        //Instantiate(GameManager.Instance.gameObject, new Vector3(0f, 0f, 0f), Quaternion.identity);
        Camera.main.SendMessage("activeMainMenuObject", false, SendMessageOptions.RequireReceiver);
        Camera.main.SendMessage("activeLevelSelectObject", true, SendMessageOptions.RequireReceiver);
    }

    void OnInstructionButtonClick()
    {
        if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
        StartCoroutine(InstructionButtonAction());
    }

    private IEnumerator InstructionButtonAction()
    {
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        Camera.main.SendMessage("activeMainMenuObject", false, SendMessageOptions.RequireReceiver);
        Camera.main.SendMessage("activeInstructionObject", true, SendMessageOptions.RequireReceiver);
    }

    void SoundToggleClick()
    {

        if (GameManager.Instance.IsSound)
        {
            GameManager.Instance.IsSound = false;
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().Stop();
            PlayerPrefs.SetInt(USERDATA_SOUND, 0);
            PlayerPrefs.Save();
        }
        else
        {
            GameManager.Instance.IsSound = true;
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().Play();
            PlayerPrefs.SetInt(USERDATA_SOUND, 1);
            PlayerPrefs.Save();
        }
    }

}

