﻿using UnityEngine;
using System.Collections;

public static class Constants
{

    public const string TAG_RED = "Red";
    public const string TAG_GREEN = "Green";
    public const string TAG_BLUE = "Blue";
    public const string TAG_YELLOW = "Yellow";
    public const string TAG_PINK = "Pink";
    public const string TAG_WHITE = "White";
    public const string TAG_BLANK = "Blank";




    public const string TAG_CUSTOM_WHITE = "CustomWhite";
    public const string TAG_GAME_CONTROLLER = "GameController";

    public const string SPRITE_BOX_WHITE = "box_white";

    public const int USERDATA_RED_CODE = 0;
    public const int USERDATA_GREEN_CODE = 1;
    public const int USERDATA_BLUE_CODE = 2;
    public const int USERDATA_YELLOW_CODE = 3;
    public const int USERDATA_PINK_CODE = 4;
    public const int USERDATA_WHITE_CODE = -1;

    public const float CELL_SIZE = 205;
    public const float CELL_PADDING = -38f;


    public const int FIVE_GRID_SIZE = 5;
    public const int SIX_GRID_SIZE = 6;
    public const int SEVEN_GRID_SIZE = 7;
    public const int EIGHT_GRID_SIZE = 8;
    public const int NINE_GRID_SIZE = 9;

	public const int FOOD_COUNT = 5;

	// SCENE IDS
	public const int GAME_SCENE = 0;
	public const int MAINMENU_SCENE = GAME_SCENE + 1;
	public const int INSTRUCTION_SCENE = MAINMENU_SCENE + 1;
	public const int LEVEL_SELECT_SCENE = INSTRUCTION_SCENE + 1;
	public const int LEVEL_COMPLETE_SCENE = LEVEL_SELECT_SCENE + 1;
	public const int GAME_OVER_SCENE = LEVEL_COMPLETE_SCENE + 1;
}
