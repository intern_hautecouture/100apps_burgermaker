﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {

	public GameObject levelSelectObject,
	levelCompleteObject,confirmationDialogObject,
	instructionObject,gameOverDialogObject;

	public AudioClip clickAudioClip;

	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (GameManager.Instance.CurrentScene == Constants.MAINMENU_SCENE)
			{
				// show confirmation dialog
				confirmationDialogObject.SetActive(true);
			}
			else if (GameManager.Instance.CurrentScene == Constants.LEVEL_SELECT_SCENE)
			{
				levelSelectObject.SendMessage("OnBackButtonClick");
			}
			else if (GameManager.Instance.CurrentScene == Constants.GAME_SCENE)
			{
				GameObject.Find("LevelObject(Clone)").SendMessage("OnMenuButtonClick");
//				topLayerObject.SendMessage("BackButtonClick");
			}
			else if (GameManager.Instance.CurrentScene == Constants.LEVEL_COMPLETE_SCENE)
			{
				levelCompleteObject.SendMessage("BackButtonClick");
			}
			else if (GameManager.Instance.CurrentScene == Constants.INSTRUCTION_SCENE)
			{
				instructionObject.SendMessage("OnBackButtonClick");
			}
			else if(GameManager.Instance.CurrentScene == Constants.GAME_OVER_SCENE)
			{
				if (GameManager.Instance.IsSound)
					AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
					                            MainMenuScript.instance.volumeSE);

				//gameOverDialogObject.SendMessage("BackButtonClick");
			}
			
		}
		
	}
}
