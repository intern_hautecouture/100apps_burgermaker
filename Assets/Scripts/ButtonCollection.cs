﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonCollection : MonoBehaviour {

	AudioSource clockAS;
	public AudioClip buttonClickSound;

	void Start(){
		clockAS = gameObject.GetComponent<AudioSource>();
	}

    void OnMenuButtonClick()
    {
		if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);   

        StartCoroutine(MenuButtonAction());

		MainMenuScript.instance.showingInstruction = false;
		MainMenuScript.instance.instructionPanel.SetActive (false);
    }

    IEnumerator MenuButtonAction()
    {
        yield return new WaitForSeconds(0.5f);
        if (!GameManager.Instance.IsLevelCompleteShowing)
        {
            Camera.main.SendMessage("activeLevelSelectObject", true, SendMessageOptions.RequireReceiver);
            Destroy(gameObject);

			clockAS.Stop ();
			
			GameObject.Find("Canvas").GetComponent<CanvasCtrl>().HideLevelText();
        }
    }


    void OnReplayButtonClick()
    {
        //Resets all cells to how they were at the beginning of the level
        GameManager.Instance.levelObject.GetComponent<GameController>().RestartCellColors();
        //if (GameManager.Instance.IsSound)
        //	AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
        //	                            MainMenuScript.instance.volumeSE);   

        //StartCoroutine(ReplayButtonAction());

        //MainMenuScript.instance.showingInstruction = false;
        //MainMenuScript.instance.instructionPanel.SetActive (false);

        //GameController.instance.goTimeText.SetActive (false);
    }

    /*IEnumerator ReplayButtonAction()
    {
        yield return new WaitForSeconds(0.5f);
        GameManager.Instance.levelObject.GetComponent<GameController>().RestartCellColors();
        //if (!GameManager.Instance.IsLevelCompleteShowing)
        //{
            // float timeLeft = GameObject.Find("LevelObject(Clone)").GetComponent<GameController>().Timer;
            //Debug.LogError("timeleft: " + timeLeft);

            //Destroy(gameObject);
            //var levelObject = Instantiate(GameManager.Instance.levelObject, new Vector3(0f, 0f, 0f), Quaternion.identity);
            //levelObject.GetComponent<GameController>().Timer = timeLeft;
			//clockAS.Stop ();


        //}
    }*/

}
