﻿using UnityEngine;
using System.Collections;

public static class CameraConstants
{
	//2048x1536
	public const float DESIGN_WIDTH = 1080f;
	public const float DESIGN_HEIGHT = 1920f;

	// 2048x1536
	public const float ASPECTRATIO_4TO3_WIDTH = 2048f;
	public const float ASPECTRATIO_4TO3_HEIGHT = 1536f;

	// 960x640
	public const float ASPECTRATIO_3TO2_WIDTH = 960f;
	public const float ASPECTRATIO_3TO2_HEIGHT = 640f;

	//1920x1080
	public const float ASPECTRATIO_16TO9_WIDTH = 1920f;
	public const float ASPECTRATIO_16TO9_HEIGHT = 1080f;

	//1024x600
	public const float ASPECTRATIO_5TO3_WIDTH = 1024f;
	public const float ASPECTRATIO_5TO3_HEIGHT = 600f;

	//1280x800
	public const float ASPECTRATIO_8TO5_WIDTH = 1280f;
	public const float ASPECTRATIO_8TO5_HEIGHT = 800f;


	// aspect ratio index 
	public const int ASPECTRATIO_3TO2 = 0;
	public const int ASPECTRATIO_16TO9 = 1;
	public const int ASPECTRATIO_5TO3 = 2;
	public const int ASPECTRATIO_8TO5 = 3;
	public const int ASPECTRATIO_4TO3 = 4;

}
