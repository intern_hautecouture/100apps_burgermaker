﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;
using NendUnityPlugin.Common;
using System.Collections.Generic;
using Facebook.Unity;

public class LevelCompleteScript : MonoBehaviour {

    public AudioClip clickAudioClip;
    public GameObject nextLevelButtonObj;

	public Texture2D shareTexture;

	GameObject goShareButton;
	tk2dSprite shareSprite;
	tk2dUIItem shareButtonUI;

	tk2dUIItem restartButton, nextButton, backButton;
	tk2dSprite restartBtnSprite, nextBtnSprite, backBtnSprite;
	tk2dButton restartBtn, nextBtn, backBtn;

	int clearedLevel;

    public GameObject buttonGuardObject;
    private GameObject buttonGuardInstance;

    void Awake()
    {
        //StartCoroutine(LoadAd());

		goShareButton = transform.Find("ShareBtnSprite").gameObject;
		shareSprite = goShareButton.GetComponent<tk2dSprite>();
		shareButtonUI = goShareButton.GetComponent<tk2dUIItem>();

		GameObject goRestart, goNext, goBack;
		goRestart = transform.Find ("RestartBtnSprite").gameObject;
		goNext = transform.Find ("NextLevelBtnSprite").gameObject;
		goBack = transform.Find ("BackButton").gameObject;

		restartButton = goRestart.GetComponent<tk2dUIItem>();
		nextButton = goNext.GetComponent<tk2dUIItem>();
		backButton = goBack.GetComponent<tk2dUIItem>();

		restartBtnSprite = goRestart.GetComponent<tk2dSprite>();
		nextBtnSprite = goNext.GetComponent<tk2dSprite>();
		backBtnSprite = goBack.GetComponent<tk2dSprite>();

		restartBtn = goRestart.GetComponent<tk2dButton>();
		nextBtn = goNext.GetComponent<tk2dButton>();
		backBtn = goBack.GetComponent<tk2dButton>();

    }

    void OnEnable()
    {
        GameManager.Instance.IsLevelCompleteShowing = true;
		GameManager.Instance.CurrentScene = Constants.LEVEL_COMPLETE_SCENE;
        if (GameManager.Instance.LevelId >= 25)
            nextLevelButtonObj.SetActive(false);
        else
            nextLevelButtonObj.SetActive(true);

        //NendAdInterstitial.Instance.Show();

		//追加(2015/8/26 原田貴広)
		transform.Find ("googleReviewMainButton").gameObject.SetActive (false);

		clearedLevel = PlayerPrefs.GetInt("Level");
    }

	void BackButtonClick()
	{
		if (buttonsOpend) {
			//Invoke("ShareButton", .5f);
			ShareButton ();

			//return;
		} else {
			if (GameManager.Instance.IsSound)
				AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
				                            MainMenuScript.instance.volumeSE);
		}

		StartCoroutine(BackButtonAction());
	}
	
	private IEnumerator BackButtonAction()
	{
		Time.timeScale = 1f;
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        Destroy(GameObject.Find("LevelObject(Clone)"));
		Camera.main.SendMessage("activeLevelCompleteObject", false, SendMessageOptions.RequireReceiver);
		Camera.main.SendMessage("activeLevelSelectObject", true, SendMessageOptions.RequireReceiver);

	}

    void NextLevelButtonClick()
    {
        if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
        StartCoroutine(NextAction());
    }

    private IEnumerator NextAction()
    {
        Time.timeScale = 1f;
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        Destroy(GameObject.Find("LevelObject(Clone)"));
        GameManager.Instance.LevelId++;
        LoadLevel();
        
    }

    void RestartButtonClick()
    {
        if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
        StartCoroutine(RestartAction());
    }

    private IEnumerator RestartAction()
    {
        Time.timeScale = 1f;
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        Destroy(GameObject.Find("LevelObject(Clone)"));
        LoadLevel();
    }

	Vector3 posFacebook = new Vector3(200, -200, 0);
	Vector3 posTwitter = new Vector3(-200, -200, 0);
	Vector3 posLine = new Vector3(0, -200, 0);

	bool buttonsOpend = false;

	public void ShareButton()
	{
		shareButtonUI.enabled = false;

		if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);

		GameObject goShareButton = transform.Find("ShareBtnSprite").gameObject;

		float transitionTime = 0.2f;

		foreach(Transform button in goShareButton.transform){

			Vector3 transitonVec = Vector3.zero;
			
			if(!buttonsOpend){
				
				switch(button.name)
				{
				case "FacebookBtnSprite":
					transitonVec = posFacebook;
					break;
				case "TwitterBtnSprite":
					transitonVec = posTwitter;
					break;
				case "LineBtnSprite":
					transitonVec = posLine;
					break;
				}
			}


			iTween.MoveTo(button.gameObject, iTween.Hash(
				"position", transitonVec, 
				"islocal", true, 
				"time", transitionTime, 
				"easetype", iTween.EaseType.linear));

		}

		Invoke ("buttonEnableChange", transitionTime);

	}

	void buttonEnableChange(){
		buttonsOpend = !buttonsOpend;

		string buttonSprite;

		if(buttonsOpend){
			buttonSprite = "btn_close";
		}else{
			buttonSprite = "btn_share";
		}
		
		shareSprite.spriteId = shareSprite.GetSpriteIdByName(buttonSprite);

		restartButton.enabled = !buttonsOpend;
		nextButton.enabled = !buttonsOpend;
		backButton.enabled = !buttonsOpend;

		restartBtnSprite.color = buttonsOpend ? 
			new Color(1,1,1,.5f): new Color(1,1,1,1);
		nextBtnSprite.color = buttonsOpend ? 
			new Color(1,1,1,.5f): new Color(1,1,1,1);
		backBtnSprite.color = buttonsOpend ? 
			new Color(1,1,1,.5f): new Color(1,1,1,1);

		restartBtn.enabled = !buttonsOpend;
		nextBtn.enabled = !buttonsOpend;
		backBtn.enabled = !buttonsOpend;

		shareButtonUI.enabled = true;
	}


	void ShareTwitterOperation()
	{

		string url = "http://www.mymaji.com/#/";

		/*
		#if UNITY_ANDROID
		url = "https://play.google.com/store/apps/details?id=com.hautecouture.BurgerMaker&hl=ja";
		#elif UNITY_IOS
		url = "https://itunes.apple.com/us/app/burgermaker/id1040047290?l=ja&ls=1&mt=8";
		#endif
		*/

		string str1 = "BurgerMakerでステージ";
		string str2 = "をクリア！次のオーダーは何かな？\n";
		string str3 = " #BurgerMaker";

		Application.OpenURL("http://twitter.com/intent/tweet?text=" + 
		                    WWW.EscapeURL(str1 + clearedLevel + str2 + url + str3));
		
		//UM_ShareUtility.TwitterShare("BurgerMakerでステージ" + clearedLevel + 
		//                             "をクリア！次のオーダーは何かな？\n" + url + " #BurgerMaker", shareTexture);

	}
	public void ShareTwitter()
	{
		if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);

		ShareTwitterOperation ();
		//Invoke ("ShareTwitterOperation", .0f);
	}

	void ShareLineOperation()
	{

		string storeURL = "http://www.mymaji.com/#/";

		/*
		#if UNITY_ANDROID
		storeURL = "https://play.google.com/store/apps/details?id=com.hautecouture.BurgerMaker&hl=ja";
		#elif UNITY_IOS
		storeURL = "https://itunes.apple.com/us/app/burgermaker/id1040047290?l=ja&ls=1&mt=8";
		#endif
		*/

		string str1 = "BurgerMakerでステージ";
		string str2 = "をクリア！次のオーダーは何かな？\n";

		string msg = str1 + clearedLevel + str2 + storeURL;
		string url = "http://line.me/R/msg/text/?" + System.Uri.EscapeUriString(msg);
		Application.OpenURL(url);

	}
	public void ShareLine()
	{
		if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);

		//Invoke ("ShareLineOperation", .0f);
		ShareLineOperation ();
	}

	public void ShareFacebook()
	{
		if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(clickAudioClip, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);

		//Invoke ("ShareFacebookOperation", .0f);
		ShareFacebookOperation ();
	}

    void ShareFacebookOperation()
    {
        if (!FB.IsInitialized)
        {
            Debug.Log("FB Not Initialized");
            FB.Init(Login);
        }
        else
        {
            Login();
        }
    }

    void Login()
    {
        //FB.ActivateApp();
        if (!FB.IsLoggedIn)
        {
            Debug.Log("login permissions");
            var perms = new List<string>() { "public_profile", "email" };
            FB.LogInWithReadPermissions(perms, LoginCallback);
            //FB.LogInWithPublishPermissions(perms, LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
    }

    System.Uri FeedLink = new System.Uri("http://www.mymaji.com/#/");
    string FeedLinkName = "";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    void LoginCallback(ILoginResult result = null)
    {
        if (FB.IsLoggedIn)
        {
            /*
			#if UNITY_ANDROID
			FeedLink = "https://play.google.com/store/apps/details?id=com.hautecouture.BurgerMaker&hl=ja";
			#elif UNITY_IOS
			FeedLink = "https://itunes.apple.com/us/app/burgermaker/id1040047290?l=ja&ls=1&mt=8";
			#endif
			*/

            string str1 = "BurgerMaker";
            string str2 = "BurgerMakerでステージ";
            string str3 = "をクリア！次のオーダーは何かな？";

            string pictureURL = "http://100apps.s3.amazonaws.com/000_Original/ShareImage/BurgerMaker.jpg";

            FeedLinkName = str1;
            FeedLinkDescription = str2 + clearedLevel + str3;

            FB.ShareLink(FeedLink, FeedLinkName, FeedLinkDescription, new System.Uri(pictureURL));
            /*FB.Feed(
                link: FeedLink,
                linkName: FeedLinkName,
                linkDescription: FeedLinkDescription,
                picture: pictureURL,
                properties: FeedProperties
                );*/
        }
    }

    private void LoadLevel()
    {
        var levelObject = Instantiate(GameManager.Instance.levelObject, new Vector3(0, 0, 0), Quaternion.identity);
        levelObject.GetComponent<GameController>().Timer = 31f;

        Camera.main.SendMessage("activeLevelCompleteObject", false, SendMessageOptions.RequireReceiver);
    }

    void OnDisable()
    {
        GameManager.Instance.IsLevelCompleteShowing = false;
    }

    private IEnumerator LoadAd()
    {
        Debug.Log("ddddddddddddddddddd");

        string apiKey = "";
        string spotId = "";

#if UNITY_IOS
		//apiKey = "1f1b9a3298c32d522703bfb674b9c3907f1436a8";
		spotId = "397831";
		Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.Gray);
#elif UNITY_ANDROID
        apiKey = "d22d63f50bcfd9f78fbe0dbec49bea5c37f1120d";
        spotId = "397838";
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif

        Handheld.StartActivityIndicator();
        yield return new WaitForSeconds(0.0f);

		//AdMobBannerInterstitial.Instance.ShowBanner ();
//		AdmobManager.ShowInterstitial ();

        NendAdInterstitial.Instance.Load();
    }


}
