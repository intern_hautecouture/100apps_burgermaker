﻿using UnityEngine;
using System.Collections;
//using NendUnityPlugin.AD;

public class LevelSelectScript : MonoBehaviour
{
    public const float LEVEL_BUTTON_SIZE = 150f;
	public const float LEVEL_BUTTON_PADDING = 50f;


    public GameObject levelBtnObject;
    public GameObject unlockBtnObject;

    public AudioClip buttonClickSound;


    private GameObject levelBtnInstance;
    private GameObject unlockBtnInstance;
    private GameObject[] levelList = new GameObject[26];
        
    private int idx;

    private int prelevel;
    private int curlevel;

    public GameObject buttonGuardObject;
    private GameObject buttonGuardInstance;


    void OnBackButtonClick()
    {
        if (GameManager.Instance.IsSound)
			AudioSource.PlayClipAtPoint(buttonClickSound, new Vector3(), 
			                            MainMenuScript.instance.volumeSE);
        StartCoroutine(BackButtonAction());
    }

    private IEnumerator BackButtonAction()
    {
        buttonGuardInstance = Instantiate(buttonGuardObject, new Vector3(0, 0, -6), Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(0.5f);
        Destroy(buttonGuardInstance);
        Camera.main.SendMessage("activeLevelSelectObject", false, SendMessageOptions.RequireReceiver);
        Camera.main.SendMessage("activeMainMenuObject", true, SendMessageOptions.RequireReceiver);
    }


    void Start()
    {
        idx = PlayerPrefs.GetInt("Level") + 1;
        Debug.Log("sdd" + idx);

        GenerateButtons(5);
        

    }

	void OnEnable()
	{
		GameManager.Instance.CurrentScene = Constants.LEVEL_SELECT_SCENE;

        idx = PlayerPrefs.GetInt("Level") + 1;

		//idx++;	コメントアウト(2015/8/26 原田貴広)

		GameObject goNendBanner = GameObject.Find ("NendAdBanner");
		if (goNendBanner != null) {
			//goNendBanner.GetComponent<NendAdBanner> ().Show ();
		}
	}

    void Update()
    {

        LevelClear(idx);

    }


    private void GenerateButtons(int size)
    {
        float startX = 140f + 15f ,startY=1350f;
        float pX = startX, pY = startY;
        int levelCounter=1;


        

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                levelBtnInstance = Instantiate(levelBtnObject, new Vector3(pX, pY, -1f), Quaternion.identity) as GameObject;
                unlockBtnInstance = Instantiate(unlockBtnObject, new Vector3(pX, pY, -2f), Quaternion.identity) as GameObject;
               
                levelBtnInstance.transform.parent = GameObject.Find("LevelSelectObject").transform;
                unlockBtnInstance.transform.parent = GameObject.Find("LevelSelectObject").transform;

                levelBtnInstance.GetComponent<LevelButton>().LevelNumber = levelCounter;
                unlockBtnInstance.GetComponent<LevelButton>().LevelNumber = levelCounter;

                levelList[levelCounter -1] = unlockBtnInstance;
                Transform[] childTransform = levelBtnInstance.transform.GetComponentsInChildren<Transform>();
                Transform[] childTransform2 = unlockBtnInstance.transform.GetComponentsInChildren<Transform>();
    
				//追加(2015/8/26 原田貴広)========================================
				//文字の影を表示するため
				for(int k=1; k<3; k++){
					childTransform[k].gameObject.GetComponent<tk2dTextMesh>().text = levelCounter.ToString();
					childTransform[k].Translate(new Vector3(0, -8, 0));

				}
				//=====================================================================ここまで

				//コメントアウト(2015/8/26 原田貴広)
				//childTransform[1].gameObject.GetComponent<tk2dTextMesh>().text = levelCounter.ToString();
				childTransform2[1].gameObject.GetComponent<tk2dTextMesh>().text = "";
                
                pX += (LEVEL_BUTTON_SIZE + LEVEL_BUTTON_PADDING);
                levelCounter++;

                
            }
            pX = startX;
            pY -= (LEVEL_BUTTON_SIZE + LEVEL_BUTTON_PADDING - 25);
        }

    }


    public void LevelClear(int no)
    {
        //Debug.Log("pre" + idx+"です");

        for (int i = 0; i < no; i++)
        {
            //if (i <= 25){
            
                if (levelList[i] != null)
                {
                    if (levelList[i].activeInHierarchy == true)
                        levelList[i].SetActive(false);
                }
           // }
        }
    }

    public void LevelCompleted(int level)
    {
        levelList[level+1].SetActive(false);

    }

	
}
