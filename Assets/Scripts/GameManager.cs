﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

    private static GameManager instance;
    private int levelId, currentScene;
    private bool isSound, isLevelCompleteShowing, isConfirmDialogShowing;
    // manages all color game object
    private List<GameObject> redCellList, blueCellList, yellowCellList, greenCellList, pinkCellList;

    private List<string> cellColorTagList, strightSpriteNameList, turnSpriteNameList;
    private List<GameObject>[] cellColorsListCollection;
    private List<Color> cellColorCodeList;
    private List<bool> colorCompletionList;

    public GameObject levelObject;

	public bool DeletePrefs;

	//public bool stageClear = false;	//追加(2015/8/26 原田貴広)

	void loadAD()
	{
		GameController.instance.loadAD ();
	}



    void Awake()
    {
        instance = this;

        PrepareEachColorObjectList();
        PrepareCellColorTagList();
        PrepareCellColorCodeList();
        PrepareEachColorCompleteBoolList();
        PrepareStraightSpriteNameList();
        PrepareTurnSpriteNameList();

		if(DeletePrefs){
			PlayerPrefs.DeleteAll();
		}
    }

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void ResetGameManager()
    {
        isLevelCompleteShowing = false;
        PrepareEachColorObjectList();
        PrepareCellColorTagList();
        PrepareCellColorCodeList();
        PrepareEachColorCompleteBoolList();
        PrepareStraightSpriteNameList();
        PrepareTurnSpriteNameList();
    }

    private void PrepareEachColorObjectList()
    {
        cellColorsListCollection = new List<GameObject>[5];
        redCellList = new List<GameObject>();
        cellColorsListCollection[0] = redCellList;
        greenCellList = new List<GameObject>();
        cellColorsListCollection[1] = greenCellList;
        blueCellList = new List<GameObject>();
        cellColorsListCollection[2] = blueCellList;
        yellowCellList = new List<GameObject>();
        cellColorsListCollection[3] = yellowCellList;
        pinkCellList = new List<GameObject>();
        cellColorsListCollection[4] = pinkCellList;
    }

    private void PrepareCellColorTagList()
    {
        cellColorTagList = new List<string>();
        cellColorTagList.Add(Constants.TAG_RED);
        cellColorTagList.Add(Constants.TAG_GREEN);
        cellColorTagList.Add(Constants.TAG_BLUE);
        cellColorTagList.Add(Constants.TAG_YELLOW);
        cellColorTagList.Add(Constants.TAG_PINK);

        //cellColorTagList.Add(Constants.TAG_WHITE);
    }

    private void PrepareCellColorCodeList()
    {
        cellColorCodeList = new List<Color>();
        cellColorCodeList.Add(Color.red);
        cellColorCodeList.Add(Color.green);
        cellColorCodeList.Add(Color.blue);
        cellColorCodeList.Add(Color.yellow);
        cellColorCodeList.Add(Color.cyan);
        //cellColorCodeList.Add(Color.white);
    }

    private void PrepareEachColorCompleteBoolList()
    {
        colorCompletionList = new List<bool>();
        for (int i = 0; i < 5; i++)
            colorCompletionList.Add(false);
    }

    private void PrepareStraightSpriteNameList()
    {
        strightSpriteNameList = new List<string>();
        strightSpriteNameList.Add("red_straight");
        strightSpriteNameList.Add("green_straight");
        strightSpriteNameList.Add("blue_straight");
        strightSpriteNameList.Add("yellow_straight");
        strightSpriteNameList.Add("pink_straight");
    }

    private void PrepareTurnSpriteNameList()
    {
        turnSpriteNameList = new List<string>();
        turnSpriteNameList.Add("red_turn");
        turnSpriteNameList.Add("green_turn");
        turnSpriteNameList.Add("blue_turn");
        turnSpriteNameList.Add("yellow_turn");
        turnSpriteNameList.Add("pink_turn");
    }

    public int CurrentScene
    {
        get
        {
            return currentScene;
        }
        set
        {
            currentScene = value;
        }
    }

    public bool IsConfirmDialogShowing
    {
        get
        {
            return isConfirmDialogShowing;
        }
        set
        {
            isConfirmDialogShowing = value;
        }
    }

    public bool IsLevelCompleteShowing
    {
        get
        {
            return isLevelCompleteShowing;
        }
        set
        {
            isLevelCompleteShowing = value;
        }
    }


    public int LevelId
    {
        get
        {
            return levelId;
        }
        set
        {
            levelId = value;
        }
    }

    public bool IsSound
    {
        get
        {
            return isSound;
        }
        set
        {
            isSound = value;
        }
    }

    public List<string> CellColorTagList
    {
        get
        {
            return cellColorTagList;
        }
    }

    public List<string> StraightSpriteNameList
    {
        get
        {
            return strightSpriteNameList;
        }
    }

    public List<string> TurnSpriteNameList
    {
        get
        {
            return turnSpriteNameList;
        }
    }

    public List<bool> ColorCompletionList
    {
        get
        {
            return colorCompletionList;
        }
    }

    public List<Color> CellColorCodeList
    {
        get
        {
            return cellColorCodeList;
        }
    }

    public List<GameObject>[] CellColorsListCollection
    {
        get
        {
            return cellColorsListCollection;
        }
    }

    public List<GameObject> RedCellList
    {
        get
        {
            return redCellList;
        }
    }

    public List<GameObject> GreenCellList
    {
        get
        {
            return greenCellList;
        }
    }

    public List<GameObject> YellowCellList
    {
        get
        {
            return yellowCellList;
        }
    }

    public List<GameObject> BlueCellList
    {
        get
        {
            return blueCellList;
        }
    }

    public List<GameObject> PinkCellList
    {
        get
        {
            return pinkCellList;
        }
    }
}
