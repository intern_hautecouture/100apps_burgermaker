﻿using UnityEngine;
using System.Collections;

public class AppController : MonoBehaviour {

    public GameObject levelSelectObject,instructionObject,mainMenuObject,levelCompleteObject,gameOverObject;

    public void activeLevelSelectObject(bool isActive)
    {
        levelSelectObject.SetActive(isActive);
    }

    public void activeMainMenuObject(bool isActive)
    {
        mainMenuObject.SetActive(isActive);
    }

    public void activeInstructionObject(bool isActive)
    {
        instructionObject.SetActive(isActive);
    }
	public void absolutelyActiveInstructionObject(bool playing)
	{
		instructionObject.SetActive(true);
		instructionObject.GetComponent<InstructionScript>().playing = playing;
	}

    public void activeLevelCompleteObject(bool isActive)
    {
        levelCompleteObject.SetActive(isActive);
    }

    //GameOverObject
    public void activeGameOverObject(bool isActive)
    {
        gameOverObject.SetActive(isActive);
    }

}
