﻿using UnityEngine;
using System.Collections;

public static class CellPosition
{

    public static Vector3[,] cell_xy;

    public static void CalculateCellPosition(int gridSize)
    {
        cell_xy = new Vector3[gridSize, gridSize];

        float idx = 0, idx_y = 0;
        float cellpadding = 0;

        
        switch(gridSize)
        {
            case 25://5マス
                idx = 0.6f;
                cellpadding = 0f;
			idx_y = 3.5f;
                break;
            case 36:
			idx = 0.72f;//0.8f;
			cellpadding = -35f;//-38f;
			idx_y = 3.42f;
                break;
     
        }

        float startX = Constants.CELL_SIZE * idx + cellpadding, 
		startY = Constants.CELL_SIZE * idx_y;//2.715f

        float pX = startX, pY = startY;

        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {

                cell_xy[i, j] = new Vector3(pX, pY, -2f);
				pX += (Constants.CELL_SIZE + cellpadding);
            }
            pX = startX;
            pY += (Constants.CELL_SIZE + cellpadding);
        }
    }

}
